#!/bin/bash
varconv() { if [ "$var" == 0 ]; then var=Rock; fi; if [ $var == 1 ]; then var=Paper; fi; if [ $var == 2 ]; then var=Scissors; fi }
until [ "$ask" == 1 ]; do echo '0) Rock'; echo '1) Paper'; echo '2) Scissors'; read -r human
cpu=$(openssl rand -hex 2000 | tr -dc '0-2'); ask=0; cpu=${cpu::1}; var=$cpu; human=${human,,}
if [ "$human" == rock ]; then human=0; elif [ $human == paper ]; then human=1; elif [ $human == scissors ]; then human=2; fi
if [ "$human" -eq "$cpu" ]; then echo; echo 'Both the CPU and you had the same answer.'
elif [[ "$human" -eq 0 && "$cpu" -eq 1 || $human -eq 1 && $cpu -eq 2 || $human -eq 2 && $cpu -eq 0 ]]; then echo 'The CPU won!'
elif [[ $cpu -eq 0 && $human -eq 1  || $cpu -eq 1 && $human -eq 2 || $cpu -eq 2 && $human -eq 0 ]]; then echo 'Human player won!'; fi
echo; varconv; echo CPU: "$var"; var=$human; varconv; echo Player: "$var"; echo; echo 'Would you like to play again?'; echo 'Y or N'; read -r a
if [ "$a" == y ]; then true; else ask=1; fi; done
