# rock-paper-scissors

Rock, Paper, Scissors written in 10 lines of bash including the shebang. Quick Setup below. I can not garuntee that the main script entirely works because it is almost 1 AM at the time of writing and I only had time to checl the ascii script for bugs.

## Features

1. Having a Cryprotgraphically Secure Pseudorandom Number Generator(CSPRNG) Using OpenSSL. After this is cuts everything not in the provided range. Then it gets the first number from that. I am not sure if it is still entirely cryptographically secure after that but it is a whole lot better than what was implemented by someone else. I could technically use /dev/urandom for a true random number but compatibility issues make it not worth the effort.
2. Only being 10 lines of code(including the shebang)
3. The script without ASCII Art is under 1 kb, while the script with ASCII Art is 1.6 kb.

## Run Online

https://replit.com/@Rockpods/sdjgfdmo3t53e - Need to setup an auto update but for now it should be manually updated fairly often

## Quick Setup

Default(Using git)

`git clone https://gitlab.com/all272/rock-paper-scissors/`<br />
`cd rock-paper-scissors`<br />
Now enter `bash main.sh` to play the version without ascii artwork or `bash ascii.sh` to play the version with ascii artwork.

## Installation

OpenSSL and Base64 are commonly preinstalled on linux machines but if it is not installed on your machine it is recommended to install them. OpenSSL is used for the CPU to generate a random number but on a maching without it change the CPU variable $RANDOM. This is not a cryptographically secure pseudorandom number generator (CSPRNG) which means nothing in Rock, Paper, Scissors but it was a fun challenge to do. Base64 is used to display ASCII art by decoding the ascii art that is encoded. This saves space and makes the code more readable so I don't have to use new line or print every line individually.

## Other

1. ASCII Art thanks to https://gist.github.com/wynand1004/b5c521ea8392e9c6bfe101b025c39abe

## Tools

https://www.shellcheck.net/# - Bash Spellcheck
