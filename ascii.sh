#!/bin/bash
varconv() { if [ "$var" == 0 ]; then var=Rock; fi; if [ $var == 1 ]; then var=Paper; fi; if [ $var == 2 ]; then var=Scissors; fi }; ascii() { if [ "$var" == 0 ] || [ $var == Rock ]; then var=$(base64 -d <<<ICAgIF9fX19fX18KLS0tJyAgIF9fX18pCiAgICAgIChfX19fXykKICAgICAgKF9fX19fKQogICAgICAoX19fXykKLS0tLl9fKF9fXyk=); fi; if [ "$var" == 1 ] || [ "$var" == Paper ]; then var=$(base64 -d <<<ICAgICBfX19fX19fCi0tLScgICAgX19fXylfX19fCiAgICAgICAgICAgX19fX19fKQogICAgICAgICAgX19fX19fXykKICAgICAgICAgX19fX19fXykKLS0tLl9fX19fX19fX18p); fi; if [ "$var" == 2 ] || [ "$var" == Scissors ]; then var=$(base64 -d <<<ICAgIF9fX19fX18KLS0tJyAgIF9fX18pX19fXwogICAgICAgICAgX19fX19fKQogICAgICAgX19fX19fX19fXykKICAgICAgKF9fX18pCi0tLS5fXyhfX18p); fi }
until [ "$ask" == 1 ]; do echo '0) Rock'; echo '1) Paper'; echo '2) Scissors'; read -r human
cpu=$(openssl rand -hex 2000 | tr -dc '0-2'); ask=0; cpu=${cpu::1}; var=$cpu; human=${human,,}
if [ "$human" == rock ]; then human=0; elif [ $human == paper ]; then human=1; elif [ $human == scissors ]; then human=2; fi
if [ "$human" == "$cpu" ]; then echo; echo 'Both the CPU and you had the same answer.'
elif [[ "$human" == 0 && "$cpu" == 1 || $human == 1 && $cpu == 2 || $human == 2 && $cpu == 0 ]]; then echo; echo 'The CPU won!'
elif [[ $cpu == 0 && $human == 1  || $cpu == 1 && $human == 2 || $cpu == 2 && $human == 0 ]]; then echo; echo 'Human player won!'; fi
echo; varconv;  echo CPU: "$var"; ascii; echo "$var"; var=$human; varconv; echo Player: "$var"; ascii; echo "$var"; echo; echo 'Would you like to play again?'; echo 'Y or N'; read -r a
if [ "$a" == y ]; then true; else ask=1; fi; done
